enum Color {RED, BLUE, YELLOW, GREEN};

const int redButtonPin = 8;
const int blueButtonPin = 9;
const int yellowButtonPin = 10;
const int greenButtonPin = 11;
const int redLedPin = 4;
const int blueLedPin = 7;
const int yellowLedPin = 5;
const int greenLedPin = 6;
const int winLedPin = 12; 
const int winPin = 3;
const int erreurPin = 2;

unsigned long long int ancientTime = 0;
long long int timeClignote = 0;
long long int timeButton = 0;
long timeError = 0;
long long int timeWin = 0;

int nbErreur = 0;
Color ledColors[4];
Color buttonColors[4];

bool isWin = false;
bool isError = false;
bool isButtonPress = false;
Color pressedButton;
int currentLevel = 0; //si il y a 1, 2, 3 ou 4 clignotement
int currentPress = 0; //si on doit presser le 1er, 2e, 3e ou 4e bouttons




void randomColor()
{
  for(int i = 0; i < 4; i++)
  {
    switch (random(0, 4)) 
    {
        case 0:
          ledColors[i] = RED;
          break;
        case 1:
          ledColors[i] = BLUE;
          break;
        case 2:
          ledColors[i] = YELLOW;
          break;
        case 3:
          ledColors[i] = GREEN;
          break;
    }
  }
}

//numéro de série avec une voyelle
void answerColor()
{
  for(int i = 0; i < 4; i++)
  {
    switch(nbErreur)
    {
      case 0:
        switch (ledColors[i]) 
        {
          case RED:
            buttonColors[i] = BLUE;
            break;
          case BLUE:
            buttonColors[i] = RED;
            break;            
          case YELLOW:
            buttonColors[i] = GREEN;
            break;
          case GREEN:
            buttonColors[i] = YELLOW;
            break;
        }
        break;

      case 1:
        switch (ledColors[i]) 
        {
          case RED:
            buttonColors[i] = YELLOW;
            break;
          case BLUE:
            buttonColors[i] = GREEN;
            break;            
          case YELLOW:
            buttonColors[i] = RED;
            break;
          case GREEN:
            buttonColors[i] = BLUE;
            break;
        }
        break;

      default: //nbErreur >= 2
        switch (ledColors[i]) 
        {
          case RED:
            buttonColors[i] = GREEN;
            break;
          case BLUE:
            buttonColors[i] = RED;
            break;            
          case YELLOW:
            buttonColors[i] = BLUE;
            break;
          case GREEN:
            buttonColors[i] = YELLOW;
            break;
        }
    }

  }
  
}


void setup() 
{
  Serial.begin(9600);
  randomSeed(analogRead(0));

  pinMode(redButtonPin, INPUT_PULLUP);
  pinMode(blueButtonPin, INPUT_PULLUP);
  pinMode(yellowButtonPin, INPUT_PULLUP);
  pinMode(greenButtonPin, INPUT_PULLUP);

  pinMode(redLedPin, OUTPUT);
  pinMode(blueLedPin, OUTPUT);
  pinMode(yellowLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);

  pinMode(winLedPin, OUTPUT);
  pinMode(winPin, INPUT);
  pinMode(erreurPin, INPUT);


  randomColor();
  answerColor();

}


void switchLight(int state, Color color)
{
  switch(color)
  {
    case RED:
      digitalWrite(redLedPin, state);
      break;
    case BLUE:
      digitalWrite(blueLedPin, state);
      break;            
    case YELLOW:
      digitalWrite(yellowLedPin, state);
      break;
    case GREEN:
      digitalWrite(greenLedPin, state);
      break;
  }
}

void clignote()
{

  if(timeClignote <= 500)
  {
    switchLight(HIGH, ledColors[0]);
  }
  else if(timeClignote <= 1000)
  {
    switchLight(LOW, ledColors[0]);
  }
  else if(timeClignote <= 1500 && currentLevel >= 1)
  {
    switchLight(HIGH, ledColors[1]);
  }
  else if(timeClignote <= 2000 && currentLevel >= 1)
  {
    switchLight(LOW, ledColors[1]);
  }
  else if(timeClignote <= 2500 && currentLevel >= 2)
  {
    switchLight(HIGH, ledColors[2]);
  }
  else if(timeClignote <= 3000 && currentLevel >= 2)
  {
    switchLight(LOW, ledColors[2]);
  }
  else if(timeClignote <= 3500 && currentLevel >= 3)
  {
    switchLight(HIGH, ledColors[3]);
  }
  else if(timeClignote <= 4000 && currentLevel >= 3)
  {
    switchLight(LOW, ledColors[3]);
  }
  else if (timeClignote >= (currentLevel + 2.5) * 1000)
  {
    timeClignote = 0;
  }

}

void verifyButton()
{
  if(buttonColors[currentPress] == pressedButton)
  {
    currentPress++;
  }
  else
  {
    Serial.println("erreur");
    answerColor();
    timeError = 0;
    currentPress = 0;
    nbErreur ++;
    isError = true;
  }
  if(currentPress == currentLevel + 1)
  {
    currentPress = 0;
    currentLevel += 1;
  }
  Serial.println(nbErreur);
}

void detectPress()
{
  int redButtonState = digitalRead(redButtonPin);
  int blueButtonState = digitalRead(blueButtonPin);
  int yellowButtonState = digitalRead(yellowButtonPin);
  int greenButtonState = digitalRead(greenButtonPin);
  if((redButtonState == LOW || blueButtonState == LOW || yellowButtonState == LOW || greenButtonState == LOW) && isButtonPress) 
  {
    timeButton = 0;
    return;
  }
  if((redButtonState == LOW || blueButtonState == LOW || yellowButtonState == LOW || greenButtonState == LOW) && timeButton <= 100)
  {
    isButtonPress = true;
    return;
  }
  if(redButtonState == LOW)
  {
    Serial.println("rouge");
    timeButton = 0;
    isButtonPress = true;
    pressedButton = RED;
    verifyButton();
  }
  else if(blueButtonState == LOW)
  {
    Serial.println("bleu");
    timeButton = 0;
    isButtonPress = true;
    pressedButton = BLUE;
    verifyButton();
  }
  else if(yellowButtonState == LOW)
  {
    Serial.println("jaune");
    timeButton = 0;
    isButtonPress = true;
    pressedButton = YELLOW;
    verifyButton();
  }
  else if(greenButtonState == LOW)
  {
    Serial.println("vert");
    timeButton = 0;
    isButtonPress = true;
    pressedButton = GREEN;
    verifyButton();
  }
  else
    isButtonPress = false;
}

void sendWin()
{
  if(timeWin <= 200)
  {
     pinMode(winPin, OUTPUT);
    digitalWrite(winPin, LOW);
  }
  else
  {
    digitalWrite(winPin, LOW);
    pinMode(winPin, INPUT);
  }
}

void sendError()
{
  if(timeError <= 200)
  {
    Serial.println(timeError);
    pinMode(erreurPin, OUTPUT);
    digitalWrite(erreurPin, LOW);
  }
  else
  {
    pinMode(erreurPin, INPUT);
    isError = false;
  }
}


unsigned long deltaTime(){
  unsigned long long tmp = ancientTime;
  ancientTime=millis();
  return ancientTime-tmp;
}

void loop()
{
  unsigned long tmp = deltaTime();
  timeWin += tmp;
  if(isWin)
  {
    pinMode(winLedPin, OUTPUT);
    digitalWrite(winLedPin, HIGH);
    sendWin();
    return;
  } 
  timeClignote += tmp;
  timeButton += tmp;
  timeError += tmp;

  
  clignote();

  detectPress();

  if(isError)
    sendError();
  

  if(currentLevel > 3)
  {
    digitalWrite(redLedPin, LOW);
    digitalWrite(blueLedPin, LOW);
    digitalWrite(greenLedPin, LOW);
    digitalWrite(yellowLedPin, LOW);
    isWin = true;
    timeWin = 0;
  }
    

}
