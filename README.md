# COON-keep-talking-and-nobody-explode

## Description projet

Le projet est de réaliser le jeu keep talking and nobody explode mais avec une "vraie" bombe. \
C'est un jeu à deux joueurs. Le but du jeu est de désamorcer une bombe en résolvant des modules. Pour résoudre les modules, il faut un joueur ayant accès à un manuel permettant de résoudre les modules et un autre joueur ayant accès à la bombe et décrivant les modules au joueur possédant le manuel pour que ce dernier lui dise les actions nécessaires pour résoudre les modules. \
Notre projet consiste à au lieu d'avoir une bombe virtuelle sur l'écran d'un des deux joueurs, d'avoir des modules réels réalisés à l'aide de cartes électroniques arduino.  


## Cablage:
## Intéractions entre les modules: 
<img src="./images/interactionreel.jpg">
<img src="./images/interaction.jpg">
Pour que la bombe soit capable de reconnaître lorsque le joeur fait une erreur il est nécéssaire que tous les modules soient connectés à la bombe. Pour cela deux pins sont réservés pour déterminer si il y a une erreur ou si un module a été résolu.\
Lorsqu'un module veut envoyer un message à la bombe le pin passe en mode output Low pendant 200 ms. 

## Module Bombe
<img src="./images/bombereel.jpg">
<img src="./images/bombe.jpg">
Le timer perd une minute lorsque une erreur est détectée. La diode verte s'allume en cas de victoire et la diode rouge lorsque le timer arrive à la valeur 0.

## Module fils

<img src="./images/filsreel.jpg">
<img src="./images/fils.jpg">
Les fils en rouge sont ceux que l'on doit couper. La diode verte s'allume lorsque le module est complété.

## Module Simon
<img src="./images/simonreel.jpg">
<img src="./images/simon.jpg">
Les interrupteurs sont des boutons de la couleur de leur led associée. Encadré en vert les pins en mode output. Et en rouge les pins en mode input pullup.

## Module Memory
<img src="./images/codereel.jpg">
<img src="./images/code.jpg">
Les boutons sont sur des pins en mode input pullup.

## Bibliothèques nécessaires pour faire marcher les modules : 

- TM1637.h (Pour les afficheurs 7 segments)

