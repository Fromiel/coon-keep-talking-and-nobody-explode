const int triggerBombePin =2;
const int resolveModulePin =3; 
const int fil1Pin = 4;     
const int fil2Pin = 5;  
const int fil3Pin = 6;
const int fil4Pin = 7;
const int fil5Pin = 8;
const int fil6Pin = 9; 
const int ledPin=12;
int fil1State = 0;
int fil2State = 0;
int fil3State = 0;
int fil4State = 0;
int fil5State = 0;
int fil6State = 0;
bool fil1IsCut = false;
bool fil2IsCut = false;
bool fil3IsCut = false;
bool fil4IsCut = false;
bool fil5IsCut = false;
bool fil6IsCut = false;
bool isWin=false;
unsigned long long ancientTime = 0;
unsigned long long timebomb=500;
unsigned long long timeWin=500;

unsigned long deltaTime(){
  unsigned long long tmp = ancientTime;
  ancientTime=millis();
  return ancientTime-tmp;
}


void setup() {
  Serial.begin(9600);
  // initialize the fils pin as an Input:
  pinMode(fil1Pin, INPUT_PULLUP);
  pinMode(fil2Pin, INPUT_PULLUP);
  pinMode(fil3Pin, INPUT_PULLUP);
  pinMode(fil4Pin, INPUT_PULLUP);
  pinMode(fil5Pin, INPUT_PULLUP);
  pinMode(fil6Pin, INPUT_PULLUP);
  // initialize the pushbutton pin as an input:
  pinMode(triggerBombePin, OUTPUT);
  //digitalWrite(triggerBombePin)
  pinMode(resolveModulePin, INPUT);
  pinMode(ledPin,OUTPUT);

  digitalWrite(ledPin,LOW);
  digitalWrite(triggerBombePin,LOW);
  digitalWrite(resolveModulePin,LOW);

}

void loop() {
  if(!isWin){
    unsigned long tmp = deltaTime();
    timebomb+=tmp;
    timeWin+=tmp;

    // read the state of the fils value:
    fil1State = digitalRead(fil1Pin);
    fil2State = digitalRead(fil2Pin);
    fil3State = digitalRead(fil3Pin);
    fil4State = digitalRead(fil4Pin);
    fil5State = digitalRead(fil5Pin);
    fil6State = digitalRead(fil6Pin);
    // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
    if(timebomb > 200){
      digitalWrite(triggerBombePin,LOW);
      pinMode(triggerBombePin, INPUT);
      if(!fil1IsCut && fil1State == HIGH){
        pinMode(triggerBombePin, OUTPUT);
        digitalWrite(triggerBombePin,LOW);
        timebomb=0;
        fil1IsCut=true;
        Serial.println("explode1");
      }
      if(!fil2IsCut && fil2State == HIGH){
        pinMode(triggerBombePin, OUTPUT);
        digitalWrite(triggerBombePin,LOW);
        timebomb=0;
        fil2IsCut=true;
        Serial.println("explod2");
      }
      if(!fil3IsCut && fil3State == HIGH){
        pinMode(triggerBombePin, OUTPUT);
        digitalWrite(triggerBombePin,LOW);
        timebomb=0;
        fil3IsCut=true;
        Serial.println("explod3");
      }
      if(!fil5IsCut && fil5State == HIGH){
        pinMode(triggerBombePin, OUTPUT);
        digitalWrite(triggerBombePin,LOW);
        timebomb=0;
        fil5IsCut=true;
        Serial.println("explod5");
      }
      if(!fil6IsCut && fil6State == HIGH){
        pinMode(triggerBombePin, OUTPUT);
        digitalWrite(triggerBombePin,LOW);
        timebomb=0;
        fil6IsCut=true;
        Serial.println("explod6");
      }
      
      
    }
    if(timeWin> 300 && fil4State == HIGH){
      timeWin=0;
    }
    if(timeWin <200){
      digitalWrite(ledPin,HIGH);
      pinMode(resolveModulePin, OUTPUT);
      digitalWrite(resolveModulePin, LOW);
      Serial.println("win");
    }
    if(timeWin >200 && timeWin <250){
      isWin=true;
      digitalWrite(resolveModulePin, LOW);
      pinMode(resolveModulePin, INPUT);
    }
  }
}
