#include <TM1637.h>

const int CLK = 7;
const int DIO = 8;

TM1637 tm(CLK,DIO);

const int errorPin = 2;
const int winPin = 3;
const int errorPin2 = 10;
const int winPin2 = 11;
const int sonorPin = 10;
const int greenLedPin = 5;
const int redLedPin = 6;
long long int totalTime = 300000;
unsigned long long int ancientTime = 0;
unsigned long time1=0;
unsigned long timeSinceLastFalseAnswer = 300;
unsigned long timeSinceLastTic = 1000;
unsigned long timeExpl = 0;
bool isExplosion = false;
bool isWin = false;
bool isEnd = false;
int nbModuleFinished = 0;
const int nbModules = 3; 
int sonorState = LOW;

void setup()
{
  Serial.begin(9600);
  pinMode(greenLedPin, OUTPUT);
  pinMode(redLedPin, OUTPUT);
  pinMode(sonorPin, OUTPUT);
  pinMode(errorPin,INPUT_PULLUP);
  pinMode(winPin,INPUT_PULLUP);

  pinMode(errorPin2,INPUT);
  pinMode(winPin2,INPUT);

  //setup digits
  tm.init();
  // set brightness; 0-7
  tm.set(2);

}


unsigned long deltaTime(){
  unsigned long long tmp = ancientTime;
  ancientTime=millis();
  return ancientTime-tmp;
}

/* Met à jour le timer*/
void setTimer(long long int time)
{
  if(time < 0 && !isExplosion)
  {
    digitalWrite(redLedPin, HIGH);
    timeSinceLastFalseAnswer = 0;
    isExplosion = true;
    tm.display(0,0);
    tm.display(1,0);
    tm.point(1);
    tm.display(2,0);
    tm.display(3,0);
  }
  else if(time > 0)
  {
    int min = totalTime / 1000;
    min /= 60;
    int sec = totalTime / 1000;
    sec -= min * 60;
    int d0 = min / 10;
    int d1 = min - d0 * 10;
    int d2 = sec / 10;
    int d3 = sec - d2 * 10; 
    tm.display(0, d0);
    tm.display(1, d1);
    tm.point(1);
    tm.display(2, d2);
    tm.display(3, d3);
  }
}

void detectSignal()
{
  int detectE = digitalRead(errorPin);
  int detectW = digitalRead(winPin);
  int detectE2 = digitalRead(errorPin2);
  int detectW2 = digitalRead(winPin2);
  if(detectE == LOW)
  {
    tone(sonorPin, 600, 500);
    totalTime -= 60000;
    timeSinceLastFalseAnswer = 0;
    Serial.println("erreur");
  }
  else if (detectW == LOW)
  {
    nbModuleFinished ++;
    isWin = nbModuleFinished >= nbModules;
    timeSinceLastFalseAnswer = 0;
    Serial.println("win");
  }

  /*if(detectE2 == HIGH)
  {
    tone(sonorPin, 600, 500);
    totalTime -= 60000;
    timeSinceLastFalseAnswer = 0;
    Serial.println("erreur");
  }
  else if (detectW2 == HIGH)
  {
    nbModuleFinished ++;
    isWin = nbModuleFinished >= nbModules;
    timeSinceLastFalseAnswer = 0;
    Serial.println("win");
  }*/

}

void makeExplosion()
{
  sonorState = random(100) > 50? HIGH : LOW;
  digitalWrite(sonorPin, sonorState);
  delayMicroseconds(100 + timeExpl);
}



void loop()
{
  if (isEnd) return;
  
  unsigned long tmp = deltaTime();
  timeSinceLastFalseAnswer += tmp;
  timeSinceLastTic += tmp;
  if (isExplosion)
  {
    timeExpl += tmp;
    if (timeSinceLastFalseAnswer < 5000)
      makeExplosion();
     else
      digitalWrite(sonorPin, LOW);
  }
  else if (isWin)
  {
    digitalWrite(greenLedPin,HIGH);
    isEnd = true;

  } 
  else if (timeSinceLastFalseAnswer > 210)
    detectSignal();

  if(!isExplosion)
  {
    //tone(sonorPin, 500);
    if(timeSinceLastTic > 1000)
    {
      tone(sonorPin, 150, 100);
      timeSinceLastTic = 0;
    }
  }

   

  totalTime -= tmp;

  setTimer(totalTime);
}
