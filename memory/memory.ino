#include <TM1637.h>

const int CLK = 10;
const int DIO = 11;

TM1637 tm(CLK,DIO);

const int errorPin = 2;
const int winPin = 3;
const int greenLedPin = 5;
const int button1Pin = 6;
const int button2Pin = 7;
const int button3Pin = 8;
const int button4Pin = 9;

unsigned long long int ancientTime = 0;
long long int timeButton = 0;
long timeError = 0;
long long int timeWin = 0;

bool isWin = false;
bool isError = false;
bool isButtonPress = false;
int pressedButton;
int currentLevel = 0;

int buttonLabels[4] = {1,3,4,2}; //exemple : buttonLabels[3] représente le chiffre sur le 4e boutton 
int digitsToPrint[5];
int positionAnswer[5];


void randomDigits()
{
    for(int i = 0; i < 5; i++)
    {
        digitsToPrint[i] = random(1,5);
    }
}

int buttonWithNumber(int n)
{
    for(int i = 0; i < 4; i++)
    {
        if (buttonLabels[i] == n)
            return i+1;
    }
    return -1;
}


int etape1()
{
    switch (digitsToPrint[0]) {
        case 1:
            return 2;
        case 2:
            return 2;
        case 3:
            return 3;
        case 4:
            return 4;
    }
}

int etape2()
{
    switch (digitsToPrint[1]) {
        case 1:
            return buttonWithNumber(4);
        case 2:
            return positionAnswer[0];
        case 3:
            return 1;
        case 4:
            return positionAnswer[0];
    }
}

int etape3()
{
    switch (digitsToPrint[2]) {
        case 1:
            return positionAnswer[1];
        case 2:
            return positionAnswer[0];
        case 3:
            return 3;
        case 4:
            return buttonWithNumber(4);
    }
}

int etape4()
{
    switch (digitsToPrint[3]) {
        case 1:
            return positionAnswer[0];
        case 2:
            return 1;
        case 3:
            return positionAnswer[1];
        case 4:
            return positionAnswer[1];
    }
}

int etape5()
{
    switch (digitsToPrint[4]) {
        case 1:
            return positionAnswer[0];
        case 2:
            return positionAnswer[1];
        case 3:
            return positionAnswer[3];
        case 4:
            return positionAnswer[2];
    }
}


void answerDigits()
{
    positionAnswer[0] = etape1();
    positionAnswer[1] = etape2();
    positionAnswer[2] = etape3();
    positionAnswer[3] = etape4();
    positionAnswer[4] = etape5();
}

void updateDigits()
{
    tm.display(2,digitsToPrint[currentLevel]);
}


void setup()
{
    Serial.begin(9600);
    randomSeed(analogRead(0));

    pinMode(greenLedPin, OUTPUT);
    pinMode(button1Pin, INPUT_PULLUP);
    pinMode(button2Pin, INPUT_PULLUP);
    pinMode(button3Pin, INPUT_PULLUP);
    pinMode(button4Pin, INPUT_PULLUP);
    pinMode(errorPin,INPUT);
    pinMode(winPin,INPUT);

    //setup digits
    tm.init();
    // set brightness; 0-7
    tm.set(2);

    randomDigits();
    answerDigits();
    updateDigits();
}

unsigned long deltaTime(){
  unsigned long long tmp = ancientTime;
  ancientTime=millis();
  return ancientTime-tmp;
}

void sendWin()
{
  if(timeWin <= 200)
  {
    pinMode(winPin, OUTPUT);
    digitalWrite(winPin, LOW);
  }
  else
  {
      digitalWrite(winPin, LOW);
    pinMode(winPin, INPUT);
  }
}

void sendError()
{
  if(timeError <= 200)
  {
    pinMode(errorPin, OUTPUT);
    digitalWrite(errorPin, LOW);
  }
  else
  {
      digitalWrite(errorPin, LOW);
    pinMode(errorPin, INPUT);
    isError = false;
  }
}


void verifyButton()
{
  if(pressedButton == positionAnswer[currentLevel]) //check if pressedButton is correct
  {
    currentLevel++;
    updateDigits();
  }
  else
  {
    Serial.println("erreur");
    randomDigits();
    answerDigits();
    timeError = 0;
    currentLevel = 0;
    isError = true;
    updateDigits();
  }
}

void detectPress()
{
    int button1State = digitalRead(button1Pin);
    int button2State = digitalRead(button2Pin);
    int button3State = digitalRead(button3Pin);
    int button4State = digitalRead(button4Pin);
    if((button1State == LOW || button2State == LOW || button3State == LOW || button4State == LOW) && isButtonPress) 
    {
        timeButton = 0;
        return;
    }
    if((button1State == LOW || button2State == LOW || button3State == LOW || button4State == LOW) && timeButton <= 100)
    {
        isButtonPress = true;
        return;
    }
    if(button1State == LOW)
    {
        Serial.println("button1");
        timeButton = 0;
        isButtonPress = true;
        pressedButton = 1;
        verifyButton();
    }
    else if(button2State == LOW)
    {
        Serial.println("button2");
        timeButton = 0;
        isButtonPress = true;
        pressedButton = 2;
        verifyButton();
    }
    else if(button3State == LOW)
    {
        Serial.println("button3");
        timeButton = 0;
        isButtonPress = true;
        pressedButton = 3;
        verifyButton();
    }
    else if(button4State == LOW)
    {
        Serial.println("button4");
        timeButton = 0;
        isButtonPress = true;
        pressedButton = 4;
        verifyButton();
    }
    else
        isButtonPress = false;
}


void loop()
{
    unsigned long tmp = deltaTime();
    timeWin += tmp;
    if(isWin)
    {
        digitalWrite(greenLedPin, HIGH);
        sendWin();
        return;
    } 

    timeButton += tmp;
    timeError += tmp;

    detectPress();

    if(isError)
        sendError();


    if(currentLevel > 4)
    {
        digitalWrite(greenLedPin, HIGH);
        isWin = true;
        timeWin = 0;
        tm.display(0,0);
        tm.display(1,0);
        tm.display(2,0);
        tm.display(3,0);
        tm.set(0);
    }
}
